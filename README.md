# Methodologie - Informatique 2023

## Liens youtube

### "Les douze mensonges du GIEC" - Christian Gérondeau (46:55)

lien : https://www.youtube.com/watch?v=0nS0P2fv7sE

retranscription :

### WCF 2021 - The Power of Youth - Naomi Seibt (17:01)

lien : https://www.youtube.com/watch?v=Ji6H1_Ui-Iw

retranscription :

###  Vincent Courtillot : "Il n'y a plus aucun réchauffement climatique" (25:18)

lien : https://www.youtube.com/watch?v=pNThA15OyFU

retranscription : 

## Autres références

https://www.frontiersin.org/articles/10.3389/fcomm.2019.00036/full

## Outils

Télécharger les sous-titres d'une vidéo youtube : https://downsub.com/